import { TestBed } from '@angular/core/testing';

import { LadoService } from './lado.service';

describe('LadoService', () => {
  let service: LadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
