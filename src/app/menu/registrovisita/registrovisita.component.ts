import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Utils from '../../utils/utils';
import { Router } from '@angular/router';
import { LineaService } from '../services/linea.service';
import Swal from 'sweetalert2'
import { VisitaService } from '../services/visita.service';


@Component({
  selector: 'app-registrovisita',
  templateUrl: './registrovisita.component.html',
  styleUrls: ['./registrovisita.component.css']
})

export class RegistrovisitaComponent implements OnInit  {

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private lineaService : LineaService,
    private visitaService : VisitaService) {  
     }

     data : any = null;
     listLinea : Array<any> = []

     registroVisita = this.fb.group({
      numDocumento : ['', Validators.required],
      nombre : ['', Validators.required],
      apellido : ['', Validators.required],
      lineaServicio : ['', Validators.required],
      fechaDesde : ['', Validators.required],
      fechaHasta : ['', Validators.required]
  });

  ngOnInit(): void {
    this.data = sessionStorage.getItem('dataUser');
    if(this.data==null){
      this.router.navigate(['/']);
    }else{
    var datJson = JSON.parse(this.data);
      this.data =  datJson;

      this.listarLinea();

      this.setValoresDefault();

    }
  }



isSubmit = false;
grabar(){

  Utils.mostrarSpinner(true);
    
  this.isSubmit = true;

  if(this.registroVisita.valid){

    var dataForm = this.registroVisita.getRawValue();

    var dataConsulta = {
      "id_linea": Number(dataForm.lineaServicio),
      "fecha_desde": Utils.stringToDateTime(dataForm.fechaDesde)+":00",
      "fecha_hasta": Utils.stringToDateTime(dataForm.fechaHasta)+":00"
    };

    let valid = true;

    this.visitaService.validarRegistroExistente(dataConsulta).subscribe((res:any)=>{

      if(res.issuccess && res.data && res.data.length>=0){
        Swal.fire({
          text: 'Ya existe la fecha reservada en esa linea',
          confirmButtonColor: '#3085d6',
          icon: 'error',
          confirmButtonText: 'Aceptar',
          allowOutsideClick: false
        })
      

      Utils.mostrarSpinner(false);
      }else{

         var registro = {
          "id_persona": this.data.id_persona,
          "id_linea": Number(dataForm.lineaServicio),
          "fecha_desde": Utils.stringToDateTime(dataForm.fechaDesde)+":00",
          "fecha_hasta": Utils.stringToDateTime(dataForm.fechaHasta)+":00",
          "estado": 0,
          "estado_registro": 1,
          "usuario_crea": this.data.id_persona,
          "fecha_registro": Utils.stringToDateTime(new Date().toISOString())
        };
    
        console.log(registro)
    
        this.visitaService.registrarVisita(registro).subscribe((res:any)=>{
          if(res.issuccess){
            
    
            Swal.fire({
              text: 'Usuario registrado correctamente',
              confirmButtonColor: '#3085d6',
              icon: 'success',
              confirmButtonText: 'Aceptar',
              allowOutsideClick: false
              
            })
    
            this.isSubmit = false;
            this.registroVisita.reset();
            this.setValoresDefault();
    
          }else{
            Swal.fire({
              text: 'Error al realizar el registro!',
              confirmButtonColor: '#3085d6',
              icon: 'error',
              confirmButtonText: 'Aceptar',
              allowOutsideClick: false
            })
          }
    
          Utils.mostrarSpinner(false);
    
        },
        err =>{
          console.log(err)
          Utils.mostrarSpinner(false);
          Swal.fire({
            text: 'Ocurrio un error, estamos trabajando para solucionarlo!',
            confirmButtonColor: '#3085d6',
            icon: 'error',
            confirmButtonText: 'Aceptar',
            allowOutsideClick: false
          })
    
        })

      }
    })
    

  }else{
    Utils.mostrarSpinner(false);

  }
}


listarLinea(){
  Utils.mostrarSpinner(true);
  this.lineaService.obtenerLineasServicios().subscribe((res:any)=>{
    if(res.issuccess && res.data){
      this.listLinea = res.data;
    }

    Utils.mostrarSpinner(false);

  })
}

lineaServicioSeleccionado(){

}

setValoresDefault(){

  
  var datosUsuario = sessionStorage.getItem('dataPersona');

  var datoJsonUsuario = null;
  if(datosUsuario!=null){
    datoJsonUsuario = JSON.parse(datosUsuario);
  }


  this.numDocumento?.setValue(datoJsonUsuario?.numero_documento)
  this.nombre?.setValue(datoJsonUsuario?.nombre)
  this.apellido?.setValue(datoJsonUsuario?.apellido)

  this.numDocumento?.disable();
  this.nombre?.disable();
  this.apellido?.disable();
}

  get numDocumento() { return this.registroVisita.get('numDocumento'); }
  get nombre(){ return this.registroVisita.get('nombre'); }
  get apellido(){ return this.registroVisita.get('apellido'); }
  get lineaServicio() { return this.registroVisita.get('lineaServicio');}
  get fechaDesde() { return this.registroVisita.get('fechaDesde'); }
  get fechaHasta() { return this.registroVisita.get('fechaHasta'); }
  

}

