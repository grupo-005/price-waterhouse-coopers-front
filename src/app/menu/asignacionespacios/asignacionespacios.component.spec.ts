import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionespaciosComponent } from './asignacionespacios.component';

describe('AsignacionespaciosComponent', () => {
  let component: AsignacionespaciosComponent;
  let fixture: ComponentFixture<AsignacionespaciosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsignacionespaciosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionespaciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
