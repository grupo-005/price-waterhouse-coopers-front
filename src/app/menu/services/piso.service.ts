import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Utils from 'src/app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class PisoService {

  constructor(
    private readonly http: HttpClient
  ) { }

  
  getPisosLista(){
    var header: any = Utils.headers();
    return this.http.get('https://localhost:44309/api/piso/listar', {headers:header});
  }
}
