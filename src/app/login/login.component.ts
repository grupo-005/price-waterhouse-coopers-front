import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import { LoginService } from './login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(  private router: Router,
                private fb: FormBuilder ,
                private readonly loginService : LoginService) { }

  ngOnInit(): void {
    
  }

  loginForm = this.fb.group({
    usuario : ['', Validators.required],
    clave: ['', Validators.required]
  });

  isSubmit : boolean = false;

  signIn(){
    this.mostrarSpinner(true);
    this.isSubmit = true;

    if(this.loginForm.valid){
      
      var inicio = {
        "usuario": this.usuario?.value,
        "password": this.clave?.value
      };
        this.loginService.login(inicio).subscribe((rest:any)=>{

          this.mostrarSpinner(false);

          if(rest.issuccess){
            
            sessionStorage.setItem('dataUser', JSON.stringify(rest.data));
            sessionStorage.setItem('token', rest.data.token)

            if(rest.data.cod_usuario=="ADMIN"){
          
              this.router.navigate(['/dashboard/registro-espacio']);  
          
            }else{
              this.router.navigate(['/dashboard/registro-usuario']);
            }
            
          }else{

              Swal.fire({
                text: 'usuario/clave incorrecto!',
                confirmButtonColor: '#3085d6',
                icon: 'error',
                confirmButtonText: 'Aceptar'
              })
      
          }

          
          
        })
    }else{
      this.mostrarSpinner(false);
    }
  }

  get usuario() { return this.loginForm.get('usuario'); }
  get clave() { return this.loginForm.get('clave'); }


  mostrarSpinner(mostrar: boolean){
    var doc = document.getElementById('modalSpinner');
      if(doc!=null){
        doc.style.display=mostrar?'block':'none';
      }
  }

}
