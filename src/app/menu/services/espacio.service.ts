import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Utils from 'src/app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class EspacioService {

  constructor(
    private readonly http: HttpClient
  ) { }

  registrar(data: any){
    var header: any = Utils.headers();
    return this.http.post('https://localhost:44309/api/espacio/registrar', data, {headers:header});
  }

  desasignar(idVisita: any){
    var header: any = Utils.headers();
    return this.http.get('https://localhost:44309/api/espacio/desasignar?idVisita='+idVisita, {headers:header});
  }
}
