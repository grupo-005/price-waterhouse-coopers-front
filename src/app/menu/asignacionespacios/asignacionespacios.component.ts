import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { timeout } from 'rxjs';
import Utils from 'src/app/utils/utils';
import Swal from 'sweetalert2'
import { AsientoService } from '../services/asiento.service';
import { CubiculoService } from '../services/cubiculo.service';
import { LadoService } from '../services/lado.service';
import { PisoService } from '../services/piso.service';
import { VisitaService } from '../services/visita.service';
import { Router } from '@angular/router';
import { EspacioService } from '../services/espacio.service';

@Component({
  selector: 'app-asignacionespacios',
  templateUrl: './asignacionespacios.component.html',
  styleUrls: ['./asignacionespacios.component.css']
})
export class AsignacionespaciosComponent implements OnInit {

  constructor(
    private router: Router,
    private espacioService : EspacioService,
    private fb: FormBuilder,
    private visitaService : VisitaService,
    private asientoService: AsientoService,
    private cubiculoService : CubiculoService,
    private ladoService : LadoService,
    private pisoService : PisoService) { }

  ngOnInit(): void {

    /*this.asientoService.__getProjects().subscribe((rest:any)=>{
      console.log(rest);
    })*/

    this.data = sessionStorage.getItem('dataUser');
    if(this.data==null){
      this.router.navigate(['/']);
    }else{
    var datJson = JSON.parse(this.data);
      this.data =  datJson;

      this.listarRegistros();

      this.listarPisos();
  
      this.listarLados();
  
      this.listarCubiculos();
  
      this.listarAsientos();

    }




  }

  data : any = null;
  
  registrofirma = this.fb.group({
    piso : ['', Validators.required],
    lado : ['', Validators.required],
    cubiculo : ['', Validators.required],
    asiento: ['', Validators.required]
  });

  listaAsientos:Array<any>  = [];
  listaPisos:Array<any>  = [];
  listaCubiculos:Array<any>  = [];
  listaLados:Array<any>  = [];

  listaElementos:Array<any>  = [];



  /*
  {
      "numDocumento":"12345678",
      "nombre":"Lom-c",
      "apellido":"Sanchez",
      "desde":"2022-02-05 08:00:00",
      "hasta":"2022-02-05 13:00:00",
      "piso":"P1",
      "lado":"D",
      "cubiculo":"C1",
      "asiento":"1",
      "asignado":true
    },{
      "numDocumento":"12345679",
      "nombre":"Jhon",
      "apellido":"Melgar",
      "desde":"2022-02-05 08:00:00",
      "hasta":"2022-02-05 13:00:00",
      "piso":"P1",
      "lado":"D",
      "cubiculo":"C1",
      "asiento":"1",
      "asignado":false
    }
  */

    persona:any = null
    
    /*{
      "numDocumento":"12345678",
      "nombre":"Lom-c",
      "apellido":"Sanchez",
      "desde":"2022-02-05 08:00:00",
      "hasta":"2022-02-05 13:00:00",
      "piso":"P1",
      "lado":"D",
      "cubiculo":"C1",
      "asiento":"1",
      "asignado":true
    };*/

    isSubmit = false;

    listarRegistros(){
      this.visitaService.listarSolicitudesAsignacion().subscribe((res:any)=>{
        if(res.issuccess){
          this.listaElementos = res.data;
        }
      })
    }

    listarPisos(){
      this.pisoService.getPisosLista().subscribe((res:any)=>{
        if(res.issuccess){
          this.listaPisos = res.data;
        }
      })
    }

    listarLados(){
      this.ladoService.getLadoLista().subscribe((res:any)=>{
        if(res.issuccess){
          this.listaLados = res.data;
        }
      })
    }

    listarCubiculos(){
      this.cubiculoService.getCubiculosLista().subscribe((res:any)=>{
        if(res.issuccess){
          this.listaCubiculos = res.data;
        }
      })
    }

    listarAsientos(){
      this.asientoService.getAsientosLista().subscribe((res:any)=>{
        if(res.issuccess){
          this.listaAsientos = res.data;
        }
      })
    }

    desasignar(item:any){

      Utils.mostrarSpinner(true);    

      Swal.fire({
        text: "Esta seguro?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI'
      }).then((result) => {
        if (result.isConfirmed) {
  
          /*this.listaElementos.forEach(data=>{
            if(data.numero_documento == item.numero_documento){
                data.id_firma_espacio = 0}
          });*/
          this.espacioService.desasignar(item.id_visita).subscribe((res:any)=>{

            Utils.mostrarSpinner(false);

            if(res.issuccess){
              Swal.fire(
                'Desasignado!',
                '',
                'success'
              )
              this.listarRegistros();
            }else{
              Swal.fire(
                'Error al desasignar!',
                '',
                'error'
              )
            }
          })

          

        }else{
          Utils.mostrarSpinner(false);
        }
      })

    }

    cancelar(){
      Utils.mostrarModal(false,"modalAsignarEspacio")
    }

    asignar(item:any){
      this.persona =  item;
      Utils.mostrarModal(true,"modalAsignarEspacio")
      
    }

    grabar(item:any){

      Utils.mostrarSpinner(true);
      
      
      this.isSubmit = true;

      if(this.registrofirma.valid){

        Utils.mostrarModal(false,"modalAsignarEspacio")

        
        console.log(this.registrofirma.value)
        var formData = {
          "id_visita": this.persona?.id_visita,
          "id_piso": Number(this.registrofirma.value.piso),
          "id_cubiculo": Number(this.registrofirma.value.cubiculo),
          "id_asiento": Number(this.registrofirma.value.asiento),
          "id_lado": Number(this.registrofirma.value.lado),
          "estado_registro": 1,
          "usuario_crea": this.data.id_persona,
          "fecha_registro": Utils.stringToDateTime(new Date().toISOString())
        }

        this.espacioService.registrar(formData).subscribe((res:any)=>{

         

          if(res.issuccess){

            Swal.fire({
              text: 'Información registrada correctamente',
              confirmButtonColor: '#3085d6',
              icon: 'success',
              confirmButtonText: 'Aceptar',
              allowOutsideClick: false
              })

              this.listarRegistros();

          }

          Utils.mostrarSpinner(false);
          this.registrofirma.reset();
          this.isSubmit = false;
        })

      }else{

        Utils.mostrarSpinner(false);
      }

      

     

    }



    get piso() { return this.registrofirma.get('piso'); }
    get lado() { return this.registrofirma.get('lado'); }
    get cubiculo() { return this.registrofirma.get('cubiculo'); }
    get asiento() { return this.registrofirma.get('asiento'); }
}
