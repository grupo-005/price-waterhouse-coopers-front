import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Utils from 'src/app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class VacunaService {

  constructor(
    private readonly http: HttpClient
  ) { }

  registrovacunado(data:any) {
    var header: any = Utils.headers();
    return this.http.post('https://localhost:44309/api/registrovacuna/registrarvacuna', data, {headers:header});
  }

  obtenerTiposVacunas() {
    var header: any = Utils.headers();
    return this.http.get('https://localhost:44309/api/tipovacuna/listar', {headers:header});
  }

  obtenerVacunaByPersona(idPersona:any) {
    var header: any = Utils.headers();
    return this.http.get('https://localhost:44309/api/registrovacuna/obtenervacunapersona?idpersona='+idPersona, {headers:header});
  }

  listVacunadosReporte(filtro:any) {
    var header: any = Utils.headers();
    return this.http.post('https://localhost:44309/api/registrovacuna/listarreporte',filtro, {headers:header});
  }
  
}
