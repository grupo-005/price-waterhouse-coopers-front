import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Utils from 'src/app/utils/utils';


@Injectable({
  providedIn: 'root'
})
export class AsientoService {

  constructor(
    private readonly http: HttpClient
  ) { }

  getAsientosLista() {
    var header: any = Utils.headers();
    return this.http.get('https://localhost:44309/api/asiento/listar',{headers:header});
  }

}
