import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { RegistrousuarioComponent } from './menu/registrousuario/registrousuario.component';
import { RegistrovacunaComponent } from './menu/registrovacuna/registrovacuna.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResportevisitaComponent } from './menu/resportevisita/resportevisita.component';
import { RegistrovisitaComponent } from './menu/registrovisita/registrovisita.component';
import { RegistrofirmaComponent } from './menu/registrofirma/registrofirma.component';
import { AsignacionespaciosComponent } from './menu/asignacionespacios/asignacionespacios.component';
import { ResportevacunadosComponent } from './menu/resportevacunados/resportevacunados.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    RegistrousuarioComponent,
    RegistrovacunaComponent,
    ResportevisitaComponent,
    RegistrovisitaComponent,
    RegistrofirmaComponent,
    AsignacionespaciosComponent,
    ResportevacunadosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
