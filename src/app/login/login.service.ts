import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private readonly http: HttpClient
  ) { }

  login(dato:any) {
    return this.http.post('https://localhost:44309/api/usuario/login', dato);
  }

}
