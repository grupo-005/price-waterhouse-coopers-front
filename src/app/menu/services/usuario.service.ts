import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import Utils from 'src/app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private dataSource = new BehaviorSubject({});
  data = this.dataSource.asObservable();

  constructor() {
    
  }
  setJSONData(val: object) {
    this.dataSource.next(val);
  }
  getJSONData() {
    return this.data;
  }

}
