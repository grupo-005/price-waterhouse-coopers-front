import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AsignacionespaciosComponent } from './menu/asignacionespacios/asignacionespacios.component';
import { MenuComponent } from './menu/menu.component';
import { RegistrofirmaComponent } from './menu/registrofirma/registrofirma.component';
import { RegistrousuarioComponent } from './menu/registrousuario/registrousuario.component';
import { RegistrovacunaComponent } from './menu/registrovacuna/registrovacuna.component';
import { RegistrovisitaComponent } from './menu/registrovisita/registrovisita.component';
import { ResportevacunadosComponent } from './menu/resportevacunados/resportevacunados.component';
import { ResportevisitaComponent } from './menu/resportevisita/resportevisita.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'dashboard', component: MenuComponent,
    children: [
      { path: '', redirectTo: 'overview', pathMatch: 'full' },
      { path: 'registro-usuario', component: RegistrousuarioComponent },
      { path: 'registro-vacuna', component: RegistrovacunaComponent },
      { path: 'registro-visita', component: RegistrovisitaComponent },
      { path: 'registro-firma', component: RegistrofirmaComponent },
      { path: 'registro-espacio', component: AsignacionespaciosComponent },
      { path: 'reporte-vacunado', component: ResportevacunadosComponent },
      { path: 'reporte-visita', component: ResportevisitaComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
