import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Utils from 'src/app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  constructor(
    private readonly http: HttpClient
  ) { }

  registrarPersona(data:any) {
    var header: any = Utils.headers();
    return this.http.post('https://localhost:44309/api/persona/registrarpersona', data, {headers:header});
  }

  obtenerPersona(id:any) {
    debugger
    var header: any = Utils.headers();
    return this.http.get('https://localhost:44309/api/persona/obtenerpersona?idpersona='+id,  {headers:header});
  }
}
