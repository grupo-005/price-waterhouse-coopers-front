import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { UsuarioService } from '../services/usuario.service';
import Swal from 'sweetalert2'
import { PersonaService } from '../services/persona.service';
import Utils from '../../utils/utils';
import { formatDate } from '@angular/common' 

@Component({
  selector: 'app-registrousuario',
  templateUrl: './registrousuario.component.html',
  styleUrls: ['./registrousuario.component.css']
})
export class RegistrousuarioComponent implements OnInit {

  constructor(  private router: Router,
    private fb: FormBuilder ,
    private usuarioService : UsuarioService,
    private personaService : PersonaService) {
     }

  ngOnInit(): void {
    this.validarPagina();
  }

  registroUsuario = this.fb.group({
    numDocumento : ['', Validators.required],
    nombre : ['', Validators.required],
    apellido: ['', Validators.required],
    fecNacimiento: ['', Validators.required],
    direccion: ['', Validators.required],
    vacunado : ['', Validators.required]
  });

  isSubmit : boolean = false;
  data : any = null;

  validarPagina(){
    Utils.mostrarSpinner(true);
    this.data = sessionStorage.getItem('dataUser');
    if(this.data==null){
      Utils.mostrarSpinner(false);
      this.router.navigate(['/']);
    }else{

      var datJson = JSON.parse(this.data);
      this.data =  datJson;

      this.personaService.obtenerPersona(datJson.id_persona).subscribe((rest:any)=>{

        Utils.mostrarSpinner(false);

        if(rest.issuccess){

          if(rest.data.estado_registro==1){//ya registro dato con anterioridad
            sessionStorage.setItem('dataPersona', JSON.stringify(rest.data));

            

            this.registroUsuario.patchValue({
              numDocumento : rest.data.numero_documento,
              nombre : rest.data.nombre,
              apellido: rest.data.apellido,
             // fecNacimiento: formatDate(rest.data.fecha_nacimiento,'yyyy-MM-dd','es'),// Utils.stringToDateDDMMYYYY(rest.data.fecha_nacimiento),
              direccion: rest.data.direccion,
              vacunado : "1"
          });
          console.log(rest.data.fecha_nacimiento.substring(0,10))
  
          this.fecNacimiento?.setValue(rest.data.fecha_nacimiento.substring(0,10))
          this.fecNacimiento?.disable();
          this.numDocumento?.disable()
          this.nombre?.disable()
          this.apellido?.disable()

            /*Swal.fire({
              text: 'Usted ya ha completado este formulario!',
              confirmButtonColor: '#3085d6',
              icon: 'success',
              confirmButtonText: 'Aceptar',
              allowOutsideClick: false
              
            }).then((result) => {
              if (result.isConfirmed) {
                this.router.navigate(['/dashboard/registro-vacuna']);
              }
            })*/

            
          }else if (rest.data.estado_registro==0){// el registro no existe
            Swal.fire({
              text: 'Usted no esta autorizado para poder registrarse',
              confirmButtonColor: '#3085d6',
              icon: 'error',
              confirmButtonText: 'Aceptar',
              allowOutsideClick: false
            }).then((result) => {
              this.router.navigate(['/']);
            })
          }else{
            //2 permite registrar sus datos
          }
        }
      })
    }

  }

  grabar(){
    Utils.mostrarSpinner(true);
    this.isSubmit = true;

    var dataForm = this.registroUsuario.getRawValue();

    if(this.registroUsuario.valid){

      let estaVacunado = this.registroUsuario.value.vacunado == "1";

      if (!estaVacunado) {
        Swal.fire({
          text: 'Usted no esta autorizado para poder agendar una visita',
          confirmButtonColor: '#3085d6',
          icon: 'error',
          confirmButtonText: 'Aceptar',
          allowOutsideClick: false
        }).then((result) => {
          Utils.mostrarSpinner(false);
          //this.router.navigate(['/']);
        })
      }else{
       
        var registro = {
          "id_persona": this.data.id_persona,
          "nombre": dataForm.nombre,
          "apellido": dataForm.apellido,
          "celular": null,
          "numero_documento": dataForm.numDocumento,
          "fecha_nacimiento": Utils.stringToDate(dataForm.fecNacimiento),
          "direccion": dataForm.direccion,
          "estado_registro": 1,
          "usuario_modifica": this.data.id_persona,
          "fecha_actualizacion": Utils.stringToDateTime(new Date().toISOString())
        };

        
  
        this.personaService.registrarPersona(registro).subscribe((res:any)=>{
          if(res.issuccess){
            
            sessionStorage.setItem('dataPersona', JSON.stringify(registro));

            Swal.fire({
              text: 'Usuario registrado correctamente',
              confirmButtonColor: '#3085d6',
              icon: 'success',
              confirmButtonText: 'Aceptar',
              allowOutsideClick: false
              
            }).then((result) => {
              if (result.isConfirmed) {
                this.usuarioService.setJSONData(registro)
                this.router.navigate(['/dashboard/registro-vacuna']);
              }
            })

          }else{
            Swal.fire({
              text: 'Error al realizar el registro!',
              confirmButtonColor: '#3085d6',
              icon: 'error',
              confirmButtonText: 'Aceptar',
              allowOutsideClick: false
            })
          }

          Utils.mostrarSpinner(false);

        },
        err =>{
          console.log(err)
          Utils.mostrarSpinner(false);
          Swal.fire({
            text: 'Ocurrio un error, estamos trabajando para solucionarlo!',
            confirmButtonColor: '#3085d6',
            icon: 'error',
            confirmButtonText: 'Aceptar',
            allowOutsideClick: false
          })

        })
      }
    }else{
      Utils.mostrarSpinner(false);
    }
    

  }



  get numDocumento() { return this.registroUsuario.get('numDocumento'); }
  get nombre() { return this.registroUsuario.get('nombre'); }
  get apellido() { return this.registroUsuario.get('apellido'); }
  get fecNacimiento() { return this.registroUsuario.get('fecNacimiento'); }
  get direccion() { return this.registroUsuario.get('direccion'); }
  get vacunado() { return this.registroUsuario.get('vacunado'); }


}
