import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Utils from 'src/app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class VisitaService {

  constructor(
    private readonly http: HttpClient
  ) { }

  registrarVisita(data:any) {
    var header: any = Utils.headers();
    return this.http.post('https://localhost:44309/api/visita/registrarvisita', data, {headers:header});
  }

  validarRegistroExistente(data:any) {
    var header: any = Utils.headers();
    return this.http.post('https://localhost:44309/api/visita/validarrangofecha', data, {headers:header});
  }

  listarSolicitudesAsignacion() {
    var header: any = Utils.headers();
    return this.http.get('https://localhost:44309/api/visita/listarsolicitudasignacion', {headers:header});
  }

  listarSolicitudesAsignadosReporte(filtros:any) {
    var header: any = Utils.headers();
    return this.http.post('https://localhost:44309/api/visita/listarreporte', filtros, {headers:header});
  }

}
