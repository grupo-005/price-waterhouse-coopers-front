import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import { UsuarioService } from '../services/usuario.service';


@Component({
  selector: 'app-registrofirma',
  templateUrl: './registrofirma.component.html',
  styleUrls: ['./registrofirma.component.css']
})
export class RegistrofirmaComponent implements OnInit {

  constructor(  private router: Router,
    private fb: FormBuilder) { }

  ngOnInit(): void {
  }


  registrofirma = this.fb.group({
    piso : ['', Validators.required],
    lado : ['', Validators.required],
    cubiculo : ['', Validators.required],
    asiento: ['', Validators.required]
  });

  isSubmit : boolean = false;
  listaElementos:Array<any> = []
  
  grabar(){

    this.isSubmit = true;
debugger
    if(this.registrofirma.valid){

      Swal.fire({
        text: 'Información registrada correctamente',
        confirmButtonColor: '#3085d6',
        icon: 'success',
        confirmButtonText: 'Aceptar'
      })

      let registro = this.registrofirma.value



      this.listaElementos.push({
        "piso":registro.piso,
        "pisoDescripcion":"Piso "+registro.piso,
        "lado":registro.lado,
        "ladoDescripcion":registro.lado == "I"?"Izquierdo":"Derecho",
        "cubiculo":registro.cubiculo,
        "asiento":registro.asiento
      });
  
      this.isSubmit = false;
      this.registrofirma.reset();

      
    }
    
 

  }

  quitar(item : any){
    console.log("valor a quitar")

    Swal.fire({
      text: "Esta seguro?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI'
    }).then((result) => {
      if (result.isConfirmed) {

        if(this.eliminaRegistro(item)){
          Swal.fire(
            'Eliminado!',
            '',
            'success'
          )
        }else{

          Swal.fire(
            'Error al eliminar!',
            '',
            'error'
          )
        
        }
      }
    })

  }

  eliminaRegistro(item : any){
    
    let eliminarExitoso = false;
    
    this.listaElementos = this.listaElementos.filter(element =>
      {

        if(element.piso == item.piso && 
          element.lado == item.lado && 
          element.cubiculo == item.cubiculo && 
          element.asiento == item.asiento){
            
            eliminarExitoso = true;

            return false;

          }else{
            
            eliminarExitoso = false;

            return true;
            
          }
      });

      return eliminarExitoso;
  }

  get piso() { return this.registrofirma.get('piso'); }
  get lado() { return this.registrofirma.get('lado'); }
  get cubiculo() { return this.registrofirma.get('cubiculo'); }
  get asiento() { return this.registrofirma.get('asiento'); }



}
