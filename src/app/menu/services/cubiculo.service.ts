import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Utils from 'src/app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class CubiculoService {

  constructor(
    private readonly http: HttpClient
  ) { }

  getCubiculosLista(){
    var header: any = Utils.headers();
    return this.http.get('https://localhost:44309/api/cubiculo/listar', {headers:header});
  }
}
