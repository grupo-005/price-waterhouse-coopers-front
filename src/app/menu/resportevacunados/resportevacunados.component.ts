import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { VacunaService } from '../services/vacuna.service';
import Swal from 'sweetalert2';
import Utils from '../../utils/utils';

import jsPDF from 'jspdf';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfmake from 'html-to-pdfmake';

@Component({
  selector: 'app-resportevacunados',
  templateUrl: './resportevacunados.component.html',
  styleUrls: ['./resportevacunados.component.css']
})
export class ResportevacunadosComponent implements OnInit {

  title = 'htmltopdf';
  @ViewChild('pdfTable') pdfTable: ElementRef;

  constructor(
    private  vacunaService :  VacunaService
  ) { }

  ngOnInit(): void {
  }

  
  numDoc = null;
  nombres = null;
  apellidos = null;
  linea = null;

  listaMostrar:Array<any> = [];

  persona: any = null;

  

  buscar(){


    /*this.listaMostrar.push({
      "numDocumento":"12345678",
      "nombre":"Jhon",
      "Apellido":"Silvester Stalone",
      "numDosis":"5",
      "tipoVacuna":"Pfizer",
      "fecvacunacion":"04/04/2022",
      "lote":"10",
      "estavacunado":"SI",
      "linea":"Administracion"
    });

    this.listaMostrar.push({
        "numDocumento":"12564875",
        "nombre":"chuck norris",
        "Apellido":"Ray Norris",
        "numDosis":"5",
        "tipoVacuna":"Pfizer",
        "fecvacunacion":"05/04/2022",
        "lote":"10",
        "estavacunado":"SI",
        "linea":"Administracion"
      });*/

      this.listar();

  }

  limiarFiltro(){
    this.numDoc=null;
    this.nombres=null;
    this.apellidos=null;
    this.buscar();
  }

  listar(){
    Utils.mostrarSpinner(true);

    var filtro = {
      "filtro": this.numDoc !=null || this.nombres != null || this.apellidos != null,
      "numeroDoc": this.numDoc,
      "nombre": this.nombres,
      "apellido": this.apellidos
    }

    this.vacunaService.listVacunadosReporte(filtro).subscribe((res:any)=>{
      Utils.mostrarSpinner(false);
      if(res.issuccess){
        this.listaMostrar = res.data!=null?res.data:[];
      }else{
        Swal.fire({
          text: 'Error al realizar la busqueda!',
          confirmButtonColor: '#3085d6',
          icon: 'error',
          confirmButtonText: 'Aceptar',
          allowOutsideClick: false
        })
      }
    })
  }
  mostrar(data: any){
    this.persona = data;
    console.log(data)
  }

  descargar(data: any){
    this.persona = data;

    console.log(this.persona)

    Utils.mostrarSpinner(true);

    setTimeout( () => {
      Utils.mostrarSpinner(false);

      const doc = new jsPDF();
   
    const pdfTable = this.pdfTable.nativeElement;
   
    var html = htmlToPdfmake(pdfTable.innerHTML);
     
    const documentDefinition = { content: html };
    pdfMake.createPdf(documentDefinition).open(); 

    }, 300 );


  }

}
