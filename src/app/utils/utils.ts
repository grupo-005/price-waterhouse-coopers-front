export default class Utils {
    static stringToDate(val: string) { 
        //yyyy-mm-dd
        //anio-mes-dia
        return val.substring(0,4)+"-"+val.substring(5,7)+"-"+val.substring(8,10); 
    }
    static stringToDateTime(val: string) { 
        return val.substring(0,4)+"-"+val.substring(5,7)+"-"+val.substring(8,10)+val.substring(10,19); 
    }

    static stringToDateDDMMYYYY(val: string) { 
        //dd-MM-yyyy
        //dia - mes -anio
        //2022-04-05T00:00:00
        return val.substring(8,10)+"/"+val.substring(5,7)+"/"+val.substring(0,4);//         val.substring(0,4)+"-"+val.substring(5,7)+"-"+val.substring(8,10); 
    }



    static mostrarSpinner(mostrar: boolean){
          this.mostrarModal(mostrar, "modalSpinner");
      }

      static mostrarModal(mostrar: boolean, nameModal: string){
        var doc = document.getElementById(nameModal);
          if(doc!=null){
            doc.style.display=mostrar?'block':'none';
          }
      }

      static headers(){
        const token  = sessionStorage.getItem('token')
        const header = { Authorization: 'Bearer ' + token }
        return header;
      }

}

