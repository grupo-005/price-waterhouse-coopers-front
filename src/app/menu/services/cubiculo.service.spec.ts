import { TestBed } from '@angular/core/testing';

import { CubiculoService } from './cubiculo.service';

describe('CubiculoService', () => {
  let service: CubiculoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CubiculoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
