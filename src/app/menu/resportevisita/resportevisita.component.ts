import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { VisitaService } from '../services/visita.service';
import Swal from 'sweetalert2';
import Utils from '../../utils/utils';
 
   
import jsPDF from 'jspdf';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfmake from 'html-to-pdfmake';

@Component({
  selector: 'app-resportevisita',
  templateUrl: './resportevisita.component.html',
  styleUrls: ['./resportevisita.component.css']
})
export class ResportevisitaComponent implements OnInit {

  constructor(
    private visitaService :VisitaService
  ) { }

  ngOnInit(): void {
    this.listarDefault();
  }

  
  fecDesde=null;
  fecHasta=null;
  item : any = null;

  title = 'htmltopdf';
  @ViewChild('pdfTable') pdfTable: ElementRef;

  listaBusqueda:Array<any> = [];
  /*{"numDoc":"12345678", "trabajador":"Timoteo", "piso":"Piso 1","cubiculo":"C12","asiento":"P1C12D1"},
  {"numDoc":"13568965", "trabajador":"The Backyardigans", "piso":"Piso 2","cubiculo":"C12","asiento":"P1C12D1"}*/

  listaMostrar:Array<any> = [];

  limiarFiltro(){
    this.fecDesde=null;
    this.fecHasta=null;
    this.buscar();
  }

  buscar(){

    // console.log("desde:"+this.fecDesde+" hasta:"+this.fecHasta)

    // this.listaMostrar = this.listaBusqueda;
    this.listarDefault();

  }

  listarDefault(){

    Utils.mostrarSpinner(true);

    this.visitaService.listarSolicitudesAsignadosReporte({
      "filtro": (this.fecDesde!=null || this.fecHasta != null),   
      "fecha_desde": Utils.stringToDate(this.fecDesde!=null?this.fecDesde:"2000/01/01"), 
      "fecha_hasta":  Utils.stringToDate(this.fecHasta!=null?this.fecHasta:"2000/01/01")
    }).subscribe((res:any)=>{
      Utils.mostrarSpinner(false);
      if(res.issuccess){
        this.listaBusqueda = res.data!=null?res.data:[];;
        this.listaMostrar = res.data!=null?res.data:[];;
      }else{
        Swal.fire({
          text: 'Error al realizar la busqueda!',
          confirmButtonColor: '#3085d6',
          icon: 'error',
          confirmButtonText: 'Aceptar',
          allowOutsideClick: false
        })
      }
    })
  }

  descargar(data: any){
    this.item =data;

    console.log(this.item)

    Utils.mostrarSpinner(true);

    setTimeout( () => {
      Utils.mostrarSpinner(false);

      const doc = new jsPDF();
   
    const pdfTable = this.pdfTable.nativeElement;
   
    var html = htmlToPdfmake(pdfTable.innerHTML);
     
    const documentDefinition = { content: html };
    pdfMake.createPdf(documentDefinition).open(); 

    }, 300 );

    

  }
}
