import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import { UsuarioService } from '../services/usuario.service';
import { VacunaService } from '../services/vacuna.service';
import Utils from '../../utils/utils';

@Component({
  selector: 'app-registrovacuna',
  templateUrl: './registrovacuna.component.html',
  styleUrls: ['./registrovacuna.component.css']
})
export class RegistrovacunaComponent implements OnInit {

  constructor(  private router: Router,
    private fb: FormBuilder,
    private usuarioService : UsuarioService,
    private vacunaService: VacunaService) {

      usuarioService.getJSONData().subscribe(data=>{
        console.log("recibe valores del otro componente")
        console.log(data)
      })

     }

     data : any = null;

  ngOnInit(): void {
    

    this.data = sessionStorage.getItem('dataUser');
    if(this.data==null){
      this.router.navigate(['/']);
    }else{
    var datJson = JSON.parse(this.data);
      this.data =  datJson;

      this.validarSiyaRegistro();

      this.listarvacunas();

    }
  }

  

  registrovacuna = this.fb.group({
    numDosis : ['', Validators.required],
    tipovacuna : ['', Validators.required],
    especificacion : ['', Validators.required],
    fecVacunacion: ['', Validators.required],
    lote: ['', Validators.required]
  });

  listavacunas:Array<any> = []; 
  isSubmit : boolean = false;
  mostrarEspecifiqueVacuna : boolean = false;

  validarSiyaRegistro(){
    Utils.mostrarSpinner(true);
    this.vacunaService.obtenerVacunaByPersona(this.data.id_persona).subscribe((res:any)=>{
      Utils.mostrarSpinner(false);
      if(res.issuccess && res.data!=null){

        Swal.fire({
              text: 'Usted ya ha registrado su vacuna en este formulario!',
              confirmButtonColor: '#3085d6',
              icon: 'success',
              confirmButtonText: 'Aceptar',
              allowOutsideClick: false
              
            }).then((result) => {
              if (result.isConfirmed) {
                this.router.navigate(['/dashboard/registro-visita']);
              }
            })

      }
    })
  }

  listarvacunas(){
    Utils.mostrarSpinner(true);
    this.vacunaService.obtenerTiposVacunas().subscribe((res:any)=>{
      if(res.issuccess && res.data){
        this.listavacunas = res.data;
      }

      Utils.mostrarSpinner(false);

    })
  }

  tipoVacunaSeleccionado(){

    var selec = this.listavacunas.filter(d=>d.id_tipo_vacuna==this.registrovacuna.value.tipovacuna);

    

    this.mostrarEspecifiqueVacuna = selec.length>0?selec[0].estado_registro == "2":false;
    if(this.mostrarEspecifiqueVacuna){
      this.especificacion?.setValidators([Validators.required]);
    }else{
      this.especificacion?.setValidators(null);
    }

    this.especificacion?.updateValueAndValidity();

    console.log(this.registrovacuna.value)
  }
  
  grabar(){

    Utils.mostrarSpinner(true);
    
    this.isSubmit = true;

    if(this.registrovacuna.valid){

      var registro = {
        "vacunado": 1,
        "numero_dosis": this.registrovacuna.value.numDosis,
        "id_tipo_vacuna":Number( this.registrovacuna.value.tipovacuna),
        "descripcion_otro_tipo": this.registrovacuna.value.especificacion,
        "fecha_vacunacion": Utils.stringToDate(this.registrovacuna.value.fecVacunacion),
        "lote": this.registrovacuna.value.lote,
        "id_persona": this.data.id_persona,
        "estado_registro": 1,
        "usuario_crea": this.data.id_persona,
        "fecha_registro": Utils.stringToDateTime(new Date().toISOString())
      };

      this.vacunaService.registrovacunado(registro).subscribe((res:any)=>{

        if(res.issuccess){
            
        

          Swal.fire({
            text: 'Información registrada correctamente',
            confirmButtonColor: '#3085d6',
            icon: 'success',
            confirmButtonText: 'Aceptar',
            allowOutsideClick: false
            }).then((result) => {
              if (result.isConfirmed) {
                this.router.navigate(['/dashboard/registro-visita']);
              }
            })

        }else{
          Swal.fire({
            text: 'Error al realizar el registro!',
            confirmButtonColor: '#3085d6',
            icon: 'error',
            confirmButtonText: 'Aceptar',
            allowOutsideClick: false
          })
        }

        Utils.mostrarSpinner(false);

      })
      
      

    }else{
      Utils.mostrarSpinner(false);
    }

    console.log(this.registrovacuna.value)

  }

  get numDosis() { return this.registrovacuna.get('numDosis'); }
  get tipovacuna() { return this.registrovacuna.get('tipovacuna'); }
  get especificacion() { return this.registrovacuna.get('especificacion'); }
  get fecVacunacion() { return this.registrovacuna.get('fecVacunacion'); }
  get lote() { return this.registrovacuna.get('lote'); }

}
