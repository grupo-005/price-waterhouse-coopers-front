import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Utils from 'src/app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class LadoService {

  constructor(
    private readonly http: HttpClient
  ) { }

  
  getLadoLista(){
    var header: any = Utils.headers();
    return this.http.get('https://localhost:44309/api/lado/listar', {headers:header});
  }
}
