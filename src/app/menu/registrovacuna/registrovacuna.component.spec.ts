import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrovacunaComponent } from './registrovacuna.component';

describe('RegistrovacunaComponent', () => {
  let component: RegistrovacunaComponent;
  let fixture: ComponentFixture<RegistrovacunaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrovacunaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrovacunaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
